# README #

This app uses a Monte Carlo method to estimate the value of pi on a Slurm-managed
cluster. 

Once you have cloned the repository, build the project via the `make` command. Then,
begin execution of the app via the `sbatch pi.job` command. 